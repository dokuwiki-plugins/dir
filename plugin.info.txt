# General Plugin Info do not edit
base   dir
author Jacobus Geluk, Matthias Schulte, Markus Gschwendt
email  dokuwiki@lupo49.de
date   2023-07-24
name   dir
desc   Show content of current namespace, including sub namespaces and/or parent/sibling namespaces, in a table or list.
url    http://www.dokuwiki.org/plugin:dir
