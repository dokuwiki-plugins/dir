# DokuWiki Plugin: dir

## Fork by runout

Includes some enhancements:

* accept the argument `namespacename` to show the namespaces name instead of just "start".
* several bug fixes
* PHP 8.2

## Description

Renders a menu as table or tree from all or specified namespaces.

## Links

- https://gitlab.com/dokuwiki-plugins/dir
- [Doku](https://www.dokuwiki.org/plugin:dir)
- [Original site](http://www.eiroca.net/doku_barcode)


## License

GPL 2 License
